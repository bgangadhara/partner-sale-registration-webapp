import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';
import PartnerSaleRegistrationServiceSdk, {
    PartnerCommercialSaleRegSynopsisView,
    PartnerCommercialSaleRegDraftView,
    UpdatePartnerCommercialSaleRegDraftReq
}from 'partner-sale-registration-draft-service-sdk';

angular
    .module('productRegistrationPricingCntrl',['ui.bootstrap']);
export default class productRegistrationPricingCntrl{
    constructor(
        $scope,
        $rootScope,
        $q,
        $uibModal,
        $location,
        $timeout,
        sessionManager, 
        identityServiceSdk,        
        partnerSaleRegistrationServiceSdk
    ) {
        var draftId=window.localStorage.getItem("draftId");

        var isFromReg= window.localStorage.getItem("isFromProdRegPage");
        var isFromEditPage= window.localStorage.getItem("isFromProdRegEditPage");

        if(isFromReg=="true") {
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            partnerSaleRegistrationServiceSdk
                                .getPartnerSaleRegistrationDraftWithDraftId(draftId, accessToken)
                                .then(
                                    response =>
                                        resolve(response)
                                )
                    ).then(PartnerCommercialSaleRegSynopsisView => {
                        $scope.loader=false;

                        $scope.productsPricingList=PartnerCommercialSaleRegSynopsisView;
                        
                        /*angular.forEach($scope.productsPricingList.compositeLineItems,function(value,ind) {                           
                            value.isChecked = true;
                        });*/
                        
                        if(isFromEditPage!=="true"){
                            
                            $timeout(function() {
                                var buttons = document.querySelectorAll("input[type='checkbox']");

                                angular.forEach(buttons ,function(value,ind) { 
                                    angular.element(value).trigger('click');
                                });
                            }, 0);
                        }
                        
                        if(isFromEditPage=="true"){

                            angular.forEach($scope.productsPricingList.compositeLineItems,function(value,ind) {
                                
                                if(!value.price){
                                    angular.forEach(value.components , function(value2,ind2) {                                    
                                        if(!value2.price){                                        
                                            value.isChecked = true;
                                        } else {
                                            value.isChecked = false;
                                        }
                                    });
                                } else {
                                    value.isChecked = true;
                                }
                                
                                $scope.productsPricingList.compositeLineItems[ind] = value;
                            });
                        }
                    });
                });
        } else {
            $scope.productsPricingList = JSON.parse(window.localStorage.getItem("prodPricingList"));
        }
        console.log("copied content",$scope.productsPricingList);
        $scope.composites={};
        $scope.checkedCompositePrice=function(priceData,index){

            var json = $scope.productsPricingList.compositeLineItems[index];
            $scope.productsPricingList.compositeLineItems[index] = json;
            $scope.composites= $scope.productsPricingList.compositeLineItems;
        };
        $scope.productRegistrationPricing_Next=function(isValid){

            /*angular.forEach($scope.composites,function(v,k){
                if(v.isChecked==true){
                    angular.forEach(v.components,function(val,key){
                        val.price=null;
                        
                        if(key == 0){
                            let price = String(v.price);
                            
                            v.price = Number(price.replace(/[^0-9\.]+/g,""));
                        }
                    })
                }
                else{
                    v.price=null;
                    
                    angular.forEach(v.components,function(val,key){
                        
                        let price = String(val.price);
                        
                        val.price = Number(price.replace(/[^0-9\.]+/g,""));
                    })
                }
            });*/
            
            angular.forEach($scope.productsPricingList.compositeLineItems,function(v,k){
                if(v.isChecked==true){
                    angular.forEach(v.components,function(val,key){
                        val.price=null;
                        
                        if(key == 0){
                            let price = String(v.price);
                            
                            if(Number(price.replace(/[^0-9\.]+/g,"")) !== 0){
                                v.price = Number(price.replace(/[^0-9\.]+/g,""));
                            } else {
                                v.price = undefined;
                                isValid = false;
                            }
                        }
                    })
                }
                else{
                    v.price=null;
                    
                    angular.forEach(v.components,function(val,key){
                        
                        let price = String(val.price);
                        
                        if(Number(price.replace(/[^0-9\.]+/g,"")) !== 0){
                            val.price = Number(price.replace(/[^0-9\.]+/g,""));
                        } else {
                            val.price = undefined;
                            isValid = false;
                        }
                    })
                }
            });
            
            angular.forEach($scope.productsPricingList.simpleLineItems,function(v,k){
                let price = String(v.price);
                            
                if(Number(price.replace(/[^0-9\.]+/g,"")) !== 0){
                    v.price = Number(price.replace(/[^0-9\.]+/g,""));
                } else {
                    v.price = undefined;
                    isValid = false;
                }
            });
            $scope.submitted=true;
            if(isValid){
                ///for update
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                partnerSaleRegistrationServiceSdk
                                    .updatePartnerCommercialSaleRegDraft($scope.productsPricingList,draftId, accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(PartnerCommercialSaleRegSynopsisView => {
                            $scope.loader=false;
                            window.localStorage.setItem("reviewList",JSON.stringify(PartnerCommercialSaleRegSynopsisView.id));
                            window.localStorage.setItem("prodPricingList",JSON.stringify($scope.productsPricingList));

                            $scope.modalInstance = $uibModal.open({
                                scope:$scope,
                                template: '<div class="modal-header"> <h4 class="modal-title">Success !</h4></div>' +
                                '<div class="modal-body">Price added successfully </div>' +
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="pricing_confirm()">Ok</button></div>',
                                size:'sm',
                                backdrop : 'static'
                            });

                        });
                    });
            }

        };
        $scope.pricing_confirm=function(){
            $location.path("/productRegistrationReview");
        };
        $scope.productRegistrationPricing_Previous=function(){
           window.localStorage.setItem("EditedDrafts",JSON.stringify($scope.productsPricingList.id));
            $location.path("/productRegistrationDraftEdit");
        }
        $scope.Back_Drafts=function(){
            $location.path("/");
        };
    }
};

productRegistrationPricingCntrl.$inject=[
    '$scope',
    '$rootScope',
    '$q',
    '$uibModal',
    '$location',
    '$timeout',
    'sessionManager',
    'identityServiceSdk',
    'partnerSaleRegistrationServiceSdk'
];