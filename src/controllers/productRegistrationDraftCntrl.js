import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';
import PartnerSaleRegistrationServiceSdk from 'partner-sale-registration-draft-service-sdk';

angular
    .module('productRegistrationDraftCntrl',['ui.bootstrap']);

export default class productRegistrationDraftCntrl{
    constructor($q,$rootScope,sessionManager,identityServiceSdk,$scope, $uibModal,$location,partnerSaleRegistrationServiceSdk) {

        $scope.EditDraft=function(EditedDraftsList){
           window.localStorage.setItem("EditedDrafts",JSON.stringify(EditedDraftsList.id));
        };
        draftsList();
        function draftsList(){
            $scope.loader=false;
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            identityServiceSdk
                                .getUserInfo(accessToken)
                                .then(
                                    userInfo =>
                                        resolve(userInfo)
                                )
                    ).then(userInfo => {
                        $q(
                            resolve =>
                                partnerSaleRegistrationServiceSdk
                                    .getPartnerCommercialSaleRegDraftWithAccountId(userInfo._account_id, accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(PartnerCommercialSaleRegSynopsisView => {
                            $scope.loader=false;
                            $scope.DraftsList = PartnerCommercialSaleRegSynopsisView;
                        });
                    });
                });

        }
        $scope.delete_Drafts=function(selectedDrfatId){
            $scope.modalInstance = $uibModal.open({
                scope:$scope,
                template: '<div class="modal-header"> <h4 class="modal-title">Warning !</h4></div>' +
                '<div class="modal-body">Do you want to Delete the Draft ?</div>' +
                '<div class="modal-footer">' +
                '<button class="btn btn-primary" type="button" ng-click="delete_confirm()">Yes</button>' +
                '<button class="btn btn-warning" type="button" ng-click="cancel_close()">No</button></div>',
                size:'sm'
            });
            $scope.delete_confirm=function(){
                console.log("selectedDrfatId",selectedDrfatId);
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                partnerSaleRegistrationServiceSdk
                                    .deletePartnerSaleRegDraftWithId(selectedDrfatId, accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(deletedDraftId => {
                            console.log(' RESPONSE in DELETE ::',deletedDraftId);
                            draftsList();
                            $scope.loader=false;
                            $scope.modalInstance.close();
                        });
                    });
            }
        };

        $scope.cancel_close=function(){
            $scope.modalInstance.dismiss('cancel');
        };
        //for Delete

        $scope.newRegistration=function(){
            $location.path('/productRegistration');
        }
    }

};

productRegistrationDraftCntrl.$inject=[
    '$q',
    '$rootScope',
    'sessionManager',
    'identityServiceSdk',
    '$scope',
    '$uibModal',
    '$location',
    'partnerSaleRegistrationServiceSdk'
];