/**
 * @class {MergeSpiffDataWithAssetsFactory}
 */

export default class MergeSpiffDataWithAssetsFactory {    
    
    mergeSpiffDataWithAssets( assetsList , spiffData ){
        
        if( assetsList.length == 1 ){
            assetsList.forEach(( value , index ) => {
                if( spiffData.simpleSerialCode.length ){
                    spiffData.simpleSerialCode.forEach( ( val , ind ) => {
                        if( value.serialNumber.substr(0,4) == val.serialNumber ){
                            value.isSpiffEligible = val.spiffEligible ? "Yes" : "No";
                        }
                    });
                } else {
                    value.isSpiffEligible = "No";
                }
            });
        }
        if( assetsList.length > 1 ){
            assetsList.forEach(( component , index1 ) => {
                if( spiffData.compositeSerialCode.length ){
                    spiffData.compositeSerialCode.forEach(( value , index2 ) => {
                        component.isSpiffEligible = value.spiffEligible ? "Yes" : "No";
                    });
                } else {
                    component.isSpiffEligible = "No";
                }
            });
        }
        
        return assetsList;
    };
    
    mergeSpiffDataWithMultipleAssets( assetsList , spiffData ){
        
        if( assetsList.simpleLineItems ){
            assetsList.simpleLineItems.forEach(function( value , index ) {
                if( spiffData.simpleSerialCode.length ){
                    spiffData.simpleSerialCode.forEach(function( val , ind ) {
                        if( value.serialNumber.substr(0,4) == val.serialNumber ){
                            value.isSpiffEligible = val.spiffEligible ? "Yes" : "No";
                        } else {
                            value.isSpiffEligible = "No";
                        }
                    });
                } else {
                    value.isSpiffEligible = "No";
                }
            });
        }
        if( assetsList.compositeLineItems ){
            assetsList.compositeLineItems.forEach(function( component , index1 ) {
                if( spiffData.compositeSerialCode.length ){
                    spiffData.compositeSerialCode.forEach(function( value , index2 ) {
                        var counter = 0;
                        component.components.forEach(function( val1 , ind1 ) {
                            value.components.forEach(function( val2 , ind2 ) {
                                if( val1.serialNumber.substr(0,4) == val2.serialNumber){
                                    counter++;
                                    if( counter == component.components.length ){
                                        counter = 0;                                        
                                        component.isSpiffEligible = value.spiffEligible ? "Yes" : "No";
                                    }
                                }
                            });
                        });
                        if(component.isSpiffEligible !== "Yes"){                                        
                            component.isSpiffEligible = "No";
                        }
                    });
                } else {
                    component.isSpiffEligible = "No";
                }
            });
        }
        
        return assetsList;
    };
    
    static mergeSpiffDataWithAssetsFactory(){
        return new MergeSpiffDataWithAssetsFactory();
    }
}

MergeSpiffDataWithAssetsFactory.mergeSpiffDataWithAssetsFactory.$inject = [];