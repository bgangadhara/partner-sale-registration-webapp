/**
 * @class {MergeEWDataWithAssetsFactory}
 */

export default class MergeEWDataWithAssetsFactory {    
    
    mergeEWDataWithAssets( assetsList , EWData ){
        
        if( assetsList.length == 1 ){
            assetsList.forEach(( value , index ) => {
                if( EWData.simpleSerialCode.length ){
                    EWData.simpleSerialCode.forEach( ( val , ind ) => {
                        value.isEWEligible = val.eweligible ? "Yes" : "No";
                    });
                } else {
                    value.isEWEligible = "No";
                }
            });
        }
        if( assetsList.length > 1 ){
            assetsList.forEach(( component , index1 ) => {
                if( EWData.compositeSerialCode.length ){
                    EWData.compositeSerialCode.forEach(( value , index2 ) => {
                        component.isEWEligible = value.eweligible ? "Yes" : "No";
                    });
                }else {
                    component.isEWEligible = "No";
                }
            });
        }
        
        return assetsList;
    };
    
    mergeEWDataWithMultipleAssets( assetsList , EWData ){
        
        if( assetsList.simpleLineItems ){
            assetsList.simpleLineItems.forEach(function( value , index ) {
                if( EWData.simpleSerialCode.length ){
                    EWData.simpleSerialCode.forEach(function( val , ind ) {
                        if( value.serialNumber.substr(0,4) == val.serialNumber ){
                            value.isEWEligible = val.eweligible ? "Yes" : "No";
                        } else {
                            value.isEWEligible = "No";
                        }
                    });
                } else {
                    value.isEWEligible = "No";
                }
            });
        }
        if( assetsList.compositeLineItems ){
            assetsList.compositeLineItems.forEach(function( component , index1 ) {
                if( EWData.compositeSerialCode.length ){
                    EWData.compositeSerialCode.forEach(function( value , index2 ) {
                        var counter = 0;
                        component.components.forEach(function( val1 , ind1 ) {
                            value.components.forEach(function( val2 , ind2 ) {
                                if( val1.serialNumber.substr(0,4) == val2.serialNumber){
                                    counter++;
                                    if( counter == component.components.length ){ 
                                        counter = 0;                                      
                                        component.isEWEligible = value.eweligible ? "Yes" : "No";
                                    }
                                }
                            });
                        });
                        if(component.isEWEligible !== "Yes"){                                        
                            component.isEWEligible = "No";
                        }
                    });
                } else {
                    component.isEWEligible = "No";
                }
            });
        }
        
        return assetsList;
    };
    
    static mergeEWDataWithAssetsFactory(){
        return new MergeEWDataWithAssetsFactory();
    }
}

MergeEWDataWithAssetsFactory.mergeEWDataWithAssetsFactory.$inject = [];