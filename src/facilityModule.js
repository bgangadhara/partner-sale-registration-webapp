import angular from 'angular';
import SessionManager from 'session-manager';
import {AddCommercialAccountReq} from 'account-service-sdk';
import {PostalAddress} from 'postal-object-model';
import {AddAccountContactReq} from 'account-contact-service-sdk';
function facilityController($scope,$rootScope, $q, $uibModal, sessionManager, iso3166Sdk, customerSegmentServiceSdk, customerBrandServiceSdk, accountServiceSdk, accountContactServiceSdk,$timeout) {

    $scope.addNewFacility={};
    $scope.loader=false;
    $q(
        resolve =>
            iso3166Sdk
                .listCountries()
                .then(
                    countries =>
                        resolve(countries)
                )
    ).then(
        countries => {
            $scope.countries = countries;
        }
    );


    $q(
        resolve =>
            sessionManager
                .getAccessToken()
                .then(
                    accessToken =>
                        resolve(accessToken)
                )
    ).then(
        accessToken => {
            $scope.loader=true;
            $q(
                resolve =>
                    customerSegmentServiceSdk
                        .getLevel1CustomerSegment(accessToken)
                        .then(
                            customerTypes =>
                                resolve(customerTypes)
                        )
            ).then(customerTypes => {
                $scope.loader=false;
                    $scope.customerTypes = customerTypes;
            });
        }
    );


    $scope.onCountrySelected = function() {
        $q(resolve =>
            iso3166Sdk
                .listSubdivisionsWithAlpha2CountryCode($scope.addNewFacility.selectedCountry.alpha2Code)
                .then(
                    regions =>
                        resolve(regions)
                )
        ).then(
            regions => {

                $scope.regions = regions;
            }
        );
    };

    $scope.onCustomerTypeSelected = function() {
        $scope.customerSubTypes=[];
        $scope.addNewFacility.selectedCustomerSubType={};
        $scope.customerSubSubTypes=[];
        $scope.addNewFacility.selectedCustomerSubSubType={};
        $scope.facilityBrands=[];
        $scope.addNewFacility.selectedFacilityBrand={};
        if($scope.addNewFacility.selectedCustomerType!=undefined){

            $scope.loader=true;
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;

                    $q(
                        resolve =>
                            customerSegmentServiceSdk
                                .getLevel2CustomerSegmentWithType($scope.addNewFacility.selectedCustomerType.id, accessToken)
                                .then(
                                    customerSubTypes =>
                                        resolve(customerSubTypes)
                                )
                    ).then(customerSubTypes => {
                        $scope.loader=false;

                        $scope.customerSubTypes = customerSubTypes;
                    });
                    $q(
                        resolve =>
                            customerBrandServiceSdk
                                .listLevel1CustomerBrandsWithSegmentId($scope.addNewFacility.selectedCustomerType.id, accessToken)
                                .then(
                                    facilityBrands =>
                                        resolve(facilityBrands)
                                )
                    ).then(facilityBrands => {
                        $scope.loader=false;

                        $scope.facilityBrands = facilityBrands;
                    });
                }
            );
        }
    };

    $scope.onCustomerSubTypeSelected = function() {
        $scope.customerSubSubTypes=[];
        $scope.addNewFacility.selectedCustomerSubSubType={};
        if($scope.addNewFacility.selectedCustomerSubType!=undefined){


            $scope.loader=true;
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $q(
                        resolve =>
                            customerSegmentServiceSdk
                                .getLevel3CustomerSegmentWithSubType($scope.addNewFacility.selectedCustomerSubType.id, accessToken)
                                .then(
                                    customerSubSubTypes =>
                                        resolve(customerSubSubTypes)
                                )
                    ).then(customerSubSubTypes => {
                        $scope.loader=false;
                        $scope.customerSubSubTypes = customerSubSubTypes;
                    });
                    $q(
                        resolve =>
                            customerBrandServiceSdk
                                .listLevel1CustomerBrandsWithSegmentId($scope.addNewFacility.selectedCustomerSubType.id, accessToken)
                                .then(
                                    facilityBrands =>
                                        resolve(facilityBrands)
                                )
                    ).then(facilityBrands => {
                        $scope.loader=false;
                        $scope.facilityBrands = facilityBrands;
                    });
                }
            );
        }
    };

    $scope.onCustomerSubSubTypeSelected = function() {
        $scope.addNewFacility.selectedFacilityBrand={};
        if($scope.addNewFacility.selectedCustomerSubSubType!=undefined){

            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            customerBrandServiceSdk
                                .listLevel1CustomerBrandsWithSegmentId($scope.addNewFacility.selectedCustomerSubSubType.id, accessToken)
                                .then(
                                    facilityBrands =>
                                        resolve(facilityBrands)
                                )
                    ).then(facilityBrands => {
                        $scope.loader=false;
                        $scope.facilityBrands = facilityBrands;
                    });
                }
            );
        }
    };

    $scope.onFacilityBrandSelected = function() {
        if($scope.addNewFacility.selectedFacilityBrand!=undefined){

            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve => {
                            var id  = '';
                            if($scope.addNewFacility.selectedCustomerSubSubType.id) {
                                id = $scope.addNewFacility.selectedCustomerSubSubType.id;
                            }else if($scope.addNewFacility.selectedCustomerSubType.id) {
                                id = $scope.addNewFacility.selectedCustomerSubType.id;
                            }else {
                                id = $scope.addNewFacility.selectedCustomerType.id
                            }

                            customerBrandServiceSdk
                                .listLevel2CustomerBrandsWithBrandId(
                                    $scope.addNewFacility.selectedFacilityBrand.id,
                                    id,
                                    accessToken)
                                .then(
                                    facilitySubBrands =>
                                        resolve(facilitySubBrands)
                                )
                        }
                    ).then(facilitySubBrands => {
                        $scope.loader=false;
                        $scope.facilitySubBrands = facilitySubBrands;
                    });
                }
            );
        }
    };

    $scope.addFacility_submit = function(isValid) {

         $scope.submitted=true;
         $scope.loader=false;
         if(isValid && $scope.isValidMail){
             $scope.loader=true;
             $q(
                 resolve =>
                     sessionManager
                         .getAccessToken()
                         .then(
                             accessToken =>
                                 resolve(accessToken)
                         )
             ).then(
                 accessToken => {
                     $scope.loader=true;

                     var postalAddress = {};
                     var add2=($scope.addNewFacility.addressLine2==undefined)? '':$scope.addNewFacility.addressLine2;
                     postalAddress.street=$scope.addNewFacility.addressLine1.concat(' '+ add2) ;
                     postalAddress.city=$scope.addNewFacility.city;
                     postalAddress.regionIso31662Code=$scope.addNewFacility.selectedRegion.code;
                     postalAddress.postalCode=$scope.addNewFacility.postalCode;
                     postalAddress.countryIso31661Alpha2Code=$scope.addNewFacility.selectedCountry.alpha2Code;

                     var addCommercialAccountRequest = new AddCommercialAccountReq(
                         $scope.addNewFacility.facilityName,
                         postalAddress,
                         $scope.addNewFacility.phone,
                         $scope.addNewFacility.selectedCustomerType.name,
                        ($scope.addNewFacility.selectedCustomerSubType==undefined)? null:$scope.addNewFacility.selectedCustomerSubType.name,
                         ($scope.addNewFacility.selectedCustomerSubSubType==undefined)? null:$scope.addNewFacility.selectedCustomerSubSubType.name,
                        // ($scope.addNewFacility.selectedFacilityBrand.name==undefined)? "No match":$scope.addNewFacility.selectedFacilityBrand.name,
                         $scope.addNewFacility.selectedFacilityBrand.name,
                         //($scope.addNewFacility.selectedFacilitySubBrand==undefined)? "No match":$scope.addNewFacility.selectedFacilitySubBrand.name
                         ($scope.addNewFacility.selectedFacilitySubBrand==undefined)? null:$scope.addNewFacility.selectedFacilitySubBrand.name
                     );

                     $q(
                         resolve =>
                             accountServiceSdk
                                 .addCommercialAccount(addCommercialAccountRequest, accessToken)
                                 .then(
                                     response =>
                                         resolve(response)
                                 )
                     ).then(response => {
                         $scope.loader=true;

                         var addAccountContactReq = new AddAccountContactReq(response,
                             $scope.addNewFacility.contact.firstName,
                             $scope.addNewFacility.contact.lastName,
                             $scope.addNewFacility.contact.phone,
                             $scope.addNewFacility.contact.email,
                             $scope.addNewFacility.selectedCountry.alpha2Code,
                             $scope.addNewFacility.selectedRegion.code);
                         $q(
                             resolve =>
                                 accountContactServiceSdk
                                     .addAccountContact(addAccountContactReq, accessToken)
                                     .then(
                                         response =>
                                             resolve(response)
                                     )
                         ).then(response => {
                             $scope.loader=false;

                             $scope.modalInstance2 = $uibModal.open({
                                 scope:$scope,
                                 template: '<div class="modal-header"> <h4 class="modal-title">Success !</h4></div>' +
                                 '<div class="modal-body">Facility added successfully</div>' +
                                 '<div class="modal-footer">' +
                                 '<button class="btn btn-primary" type="button" ng-click="ok_addFacility()">ok</button></div>',
                                 size:'sm',
                                 backdrop : 'static'
                             });
                             $scope.ok_addFacility=function(){
                                 $scope.modalInstance2.close();
                                 $scope.modalInstance.close();
                                 $rootScope.finalResponse();
                             };
                         });

                     });
                 }
             );
         }
    };

    $scope.addFacility_cancel=function(){
        $scope.submitted=false;
        $scope.addNewFacility={};
        $scope.modalInstance.dismiss('cancel');
    };
    
    $scope.validateEmail = function(){
        var email = $scope.addNewFacility.contact ? $scope.addNewFacility.contact.email : undefined;

        if(email){
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $scope.isValidMail = re.test(email);
        } 
    };
}


angular
    .module(
        "facilityModule",
        []
    )
    .controller("facilityController", facilityController);

facilityController.$inject = ["$scope","$rootScope", "$q","$uibModal","sessionManager", "iso3166Sdk", "customerSegmentServiceSdk", "customerBrandServiceSdk", "accountServiceSdk", "accountContactServiceSdk","$timeout"];
